using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum DoorType
{
    Locked,
    Unlocked
}

public class Door : MonoBehaviour
{
    public int sceneIndex;
    public DoorType doortype;
    public string tip;
    public SpriteRenderer spriteRenderer;
    public Sprite spriteOpened;
    public Sprite spriteClosed;
    [Header("For the locked door")]
    public string tipLockedDoor;
    public int keyId;
    public GameObject GameOverUi;
    public static bool GameOverUi_pause = false;

    private Animator animator;
    private Animator Animator
    {
        get
        {
            if (animator == null ) 
                animator = GetComponent<Animator>();
            return animator;
        }
    }
    
    private void OnMouseEnter()
    {
        if(GUI_GameMenu.pause)return; //Pause interaction after clicking on the menu
        if(GameOverUi_pause)return;//Pause interaction after game win
        
        if(doortype == DoorType.Locked)
            return;
        
        if(doortype == DoorType.Unlocked)
            Animator.Play("Opened"); //Door opening animation effect
            Animator.SetBool("IsOpened", true);
    }

    private void OnMouseExit()
    {
        if(GUI_GameMenu.pause)return; //Pause interaction after clicking on the menu
        if(GameOverUi_pause)return; //Pause interaction after game win
        
        if(doortype == DoorType.Locked)
            return;
        if(doortype == DoorType.Unlocked)
            Animator.Play("Closed"); //Door closing animation effect
            Animator.SetBool("IsOpened", false);
    }

    private void OnMouseDown()
    {
        if(GUI_GameMenu.pause)return; //Pause interaction after clicking on the menu
        if(GameOverUi_pause)return; //Pause interaction after game win
    
        if (doortype == DoorType.Locked) 
        { 
            //Check if keys are in inventory
            if(Inventory.Instance.idList.Contains(keyId))
            {
                //Game Over
                Instantiate(GameOverUi);
                GameOverUi_pause = true;
            }
            else
            {
               SmallTools.Instance.showTip(tipLockedDoor,0.5f); //Door will not open tip
               SmallTools.Instance.ItemShake(this.gameObject, 0.1f, 0.1f);
               ClickSounds.Instance.LockedDoor.Play();
            }          
        }
        
        if (doortype == DoorType.Unlocked)
        {
            ClickSounds.Instance.OpenDoor.Play();
            //Save scene data
            SceneController sc = GameObject.Find("SceneController").GetComponent<SceneController>();
            sc.SaveData();
            SceneManager.LoadScene(sceneIndex);
        }
    }

    private void Update()
    {
        if(GUI_GameMenu.pause)return; //Pause interaction after clicking on the menu
        if(GameOverUi_pause)return; //Pause interaction after game win

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit2D hit = Physics2D.Raycast(
                Camera.main.ScreenToWorldPoint(Input.mousePosition),
                Vector2.zero
            );
            if (hit.collider == null)
                return;
            if (hit.collider.name == this.gameObject.name)
            {
                //The event for right click
                SmallTools.Instance.showTip(tip, 0.5f);
            }
        }
    }
}
