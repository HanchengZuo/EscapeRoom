using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static void RestartGame()
    {
        Inventory.SceneData.Clear(); //Clear the scene data
        Inventory.Instance.clear(); //Clear the inventory data
    }

    public static void QuitGame()
    {
        Inventory.SceneData.Clear(); //Clear the scene data
        GameObject.Destroy(Inventory.Instance.gameObject); //Destory the inventory object
        Destroy(Bgm_InGame.Instance.gameObject); //Destroy the in-game soundtrack
    }
}
