using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class StartMenu : MonoBehaviour
{
    public Button startbtn;
    public Button insbtn;
    public Button quitbtn;
    
    // Start is called before the first frame update
    void Start()
    {
        startbtn.onClick.AddListener(()=>
        {
            SceneManager.LoadScene(1);
            ClickSounds.Instance.ButtonClick.Play();
        });

        insbtn.onClick.AddListener(()=>
        {
            this.transform.GetChild(1).gameObject.SetActive(true);
            ClickSounds.Instance.ButtonClick.Play();
        });

        quitbtn.onClick.AddListener(()=>
        {
            Application.Quit();
            ClickSounds.Instance.ButtonClick.Play();
        });
    }
}
