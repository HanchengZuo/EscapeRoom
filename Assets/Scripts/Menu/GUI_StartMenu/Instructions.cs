using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Instructions : MonoBehaviour
{
    public Button close; 

    // Start is called before the first frame update
    void Start()
    {
        close.onClick.AddListener(()=>
        {
            this.gameObject.SetActive(false); //Close the Instructions UI
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
