using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GUI_GameMenu : MonoBehaviour
{
    public static bool pause; // Judgement stopping game interaction
    public Button menubtn;
    public Button continuebtn;
    public Button restartbtn;
    public Button backmenubtn;
    public Button quitbtn;

    // Start is called before the first frame update
    void Start()
    {
        menubtn.onClick.AddListener(()=>
        {
            ClickSounds.Instance.ButtonClick.Play();
            this.transform.GetChild(0).gameObject.SetActive(
                this.transform.GetChild(0).gameObject.activeSelf ? false : true
            ); // Menu UI multi-click interaction
        });

        continuebtn.onClick.AddListener(() => 
        {
            ClickSounds.Instance.ButtonClick.Play();
            this.transform.GetChild(0).gameObject.SetActive(false); //Close menu UI
        });

        restartbtn.onClick.AddListener(() => 
        {
            ClickSounds.Instance.ButtonClick.Play();
            GameController.RestartGame(); //Clear scene data and inventory data
            Door.GameOverUi_pause = false; //Resetting the game victory pause screen
            SceneManager.LoadScene(1); //Loading the initial in-game scene
            
            //Reset Inventory interface
            Inventory[] inv = GameObject.FindObjectsOfType<Inventory>();
            foreach(var i in inv)
            {
                for(int k=0;k<5;k++)
                {
                    i.transform.GetChild(k).GetChild(0).gameObject.GetComponent<Image>().sprite=null;
                    i.transform.GetChild(k).GetChild(0).gameObject.GetComponent<Image>().color=new Color(1,1,1,0);
                }
            }
        });

        backmenubtn.onClick.AddListener(()=>
        {
            ClickSounds.Instance.ButtonClick.Play();
            GameController.QuitGame(); //Clear scene data and inventory data
            SceneManager.LoadScene(0); // //Loading the startmenu scene
            Door.GameOverUi_pause = false; //Resetting the game victory pause screen
        });

        quitbtn.onClick.AddListener(()=>
        {
            ClickSounds.Instance.ButtonClick.Play();
            Application.Quit();
        });
    }

    private void Update()
    {
        
        //Update the judgment value of the pause interaction in the GUI munu interface
        if(this.transform.GetChild(0).gameObject.activeSelf==true)
        {
            pause=true;
        }
        if(this.transform.GetChild(0).gameObject.activeSelf==false)
        {
            pause=false;
        }
        
    }
}
