using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmallTools : Singleton<SmallTools>
{
    private bool canshake=true;

    public void ItemShake(GameObject obj, float duration, float strength)
    {
        if(!canshake)return;
        StartCoroutine(Shake(obj, duration, strength));
    }

    IEnumerator Shake(GameObject obj, float duration, float strength)
    {
        Transform shacktransform = obj.transform;
        Vector3 startPosition = obj.transform.position;
        while (duration > 0)
        {
            canshake=false;
            shacktransform.position = Random.insideUnitSphere * strength + startPosition;
            duration -= Time.deltaTime;
            yield return null;
        }
        obj.transform.position = startPosition;
        canshake = true;
    }

    public void showTip(string text, float duration)
    {
        StartCoroutine(ShowTip(text, duration));
    }

    IEnumerator ShowTip(string text, float duration)
    {
        GameObject tip = GameObject.Find("Tip").transform.GetChild(0).gameObject;
        tip.GetComponent<Text>().text = text;
        while (duration > 0)
        {
            tip.SetActive(true);
            duration -= Time.deltaTime;
            yield return null;
        }
        tip.SetActive(false);
    }
}
