using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Singleton template class
public class Singleton<T> : MonoBehaviour
    where T : Component 
{
    private static T _instance;

    public static T Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType(typeof(T)) as T;
                if(_instance == null)
                {
                    GameObject obj = new GameObject();
                    obj.hideFlags = HideFlags.HideAndDontSave;
                    _instance = obj.AddComponent<T>();
                }
            }
            return _instance;
        }
    }

    private void Awake()
    {
        //Ensure that the switching scene will not instantiate the same object
        //Ensure the unique existence of the object
        DontDestroyOnLoad(Instance.transform.root.gameObject);
        if(Instance.transform.root.gameObject != this.gameObject)
            Destroy(this.gameObject);
    }
}
