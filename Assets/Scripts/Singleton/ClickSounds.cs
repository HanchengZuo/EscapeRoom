using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickSounds : Singleton<ClickSounds>
{
    public AudioSource ButtonClick;
    public AudioSource ItemClick;
    public AudioSource OpenDoor;
    public AudioSource LockedDoor;
}
