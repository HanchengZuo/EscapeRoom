using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Inventory : Singleton<Inventory>
{
    //Inventory storage dictionary, object data bound to scene data
    public static Dictionary<string, List<InventoryItem>> SceneData = new Dictionary<string, List<InventoryItem>>();

    public ArrayList idList; // Item ID list
    Sprite sprite;
    Transform img;

    // Start is called before the first frame update
    void Start()
    {
        idList = new ArrayList();
        for (int i = 0; i < 5; i++)
            this.transform.GetChild(i).GetComponent<Inv>().empty = true; //Initialize empty inventory
    }

    
    //Get the ID of the clicked item, add the item ID to the int array, and add pictures according to the number
    public void Additem(Sprite sprite, int id)
    {
        for (int i = 0; i < 5; i++)
        {
            Debug.Log("Into the inventory");
            Transform bp = this.transform.GetChild(i); //Get inventory cell
            if (bp.GetComponent<Inv>().empty == true) //Check whether the current cell is empty
            {
                img = bp.transform.GetChild(0);
                img.GetComponent<Image>().sprite = sprite; //Set picture
                img.GetComponent<Image>().color = new Color(1, 1, 1, 1);
                idList.Add(id); //Put the item ID into the array
                bp.GetComponent<Inv>().empty = false; //Set the current cell to be non empty
                break;
            }
        }
    }

    // Clear inventory data and reset inventory images
    public void clear()
    {
        if(idList.Count==0)
        return;
        idList.Clear();
        for (int i = 0; i < 5; i++)
        {
            Transform bp = this.transform.GetChild(i);
            img.GetComponent<Image>().sprite = null;
            img.GetComponent<Image>().color = new Color(1, 1, 1, 0);
            this.transform.GetChild(i).GetComponent<Inv>().empty = true;
        }
    }
}
