using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ItemType
{
    Unpickable, //Objects that cannot be picked up
    Pickable, //Objects that can be picked up
    ContainsItem, //The object contains other objects
}

public enum rightClickType
{
    textTip, //Right click object tip
}

public enum hoverOverType
{
    SHAKE, //Object shake interaction
    ImgInteraction, //Object picture switching interaction
    GameObjectInteraction, //Game Object switching inreraction
}

public class InventoryItem
{
    public int ID; //Item ID
    public bool isContain; //Whether the object contains other objects
    public bool isPick; //Whether the object is picked up
    public bool isOpen; //Whether the object is open
}

public class Item : MonoBehaviour
{

    [Header("Item ID")]
    public int ID;

    [Header("Item Type")]
    public ItemType itemType;

    [Header("Contained Item")]
    public GameObject containItem;
    public bool isContain;

    [Header("Right Click Type")]
    public rightClickType rightClick;

    [Header("The tip for left-Click")]
    public string tipLeftClick;

    [Header("The tip for right-Click")]
    public string tipRightClick;

    [Header("Mouse hovers over type")]
    public hoverOverType hoverType;

    [Header("ImageInteraction - MouseExit")]
    public Sprite Image1;

    [Header("ImageInteraction - MouseEnter")]
    public Sprite Image2;

    [Header("GameObjectInteraction- MouseExit")]
    public GameObject ObjectEnter;

    [Header("GameObjectInteraction - MouseEnter")]
    public GameObject ObjectExit;

    public bool isPick = false;

    public bool isOpen = false;

    public InventoryItem inventoryItem;

    private void Awake()
    {
        inventoryItem = new InventoryItem();
        inventoryItem.ID = ID;
        inventoryItem.isPick = isPick;
        inventoryItem.isContain = isContain;
        Debug.Log("Item Awake");
    }

    public void InitItem()
    {
        isPick = inventoryItem.isPick;
        isContain = inventoryItem.isContain;
        isOpen = inventoryItem.isOpen;
        this.transform.GetChild(0).gameObject.SetActive(!isPick);
        if(containItem != null)
            containItem.transform.GetChild(0).gameObject.SetActive(isContain);
        if(isOpen && ObjectEnter != null & ObjectExit != null){
            ObjectEnter.transform.gameObject.SetActive(isOpen);
            ObjectExit.transform.gameObject.SetActive(!isOpen);
        } 
    }

    private void OnMouseDown()
    {
        if(GUI_GameMenu.pause)return;
        if(Door.GameOverUi_pause)return;

        ClickSounds.Instance.ItemClick.Play();

        //Handling the status of objects that can be opened and closed
        if (hoverType == hoverOverType.GameObjectInteraction){
            if(isOpen == false){
                isOpen = true;
                inventoryItem.isOpen = isOpen;
                ObjectExit.transform.gameObject.SetActive(false);
                ObjectEnter.transform.gameObject.SetActive(true);
            }
            else if(isOpen == true){
                isOpen = false;
                inventoryItem.isOpen = isOpen;
                ObjectEnter.transform.gameObject.SetActive(false);
                ObjectExit.transform.gameObject.SetActive(true);
            }
        }

        if (itemType == ItemType.Pickable && !isPick)
        {
            Inventory inv = Inventory.Instance;
            if (inv.idList.Count < 5)
            {
                inv.Additem(this.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite, ID);
                this.transform.GetChild(0).gameObject.SetActive(false);
                isPick = true;
                inventoryItem.isPick = isPick;
                SmallTools.Instance.showTip(tipLeftClick, 0.5f);
            }
            else
            {
                string tip_full = "The inventory is full";
                SmallTools.Instance.showTip(tip_full, 0.5f);
                return;
            }
        }

        if (itemType == ItemType.Unpickable && !isPick)
        {
            SmallTools.Instance.showTip(tipLeftClick, 0.5f);
        }
        if (itemType == ItemType.ContainsItem && !isPick)
        {
            if(!isContain)return;
            containItem.transform.GetChild(0).gameObject.SetActive(true);
            containItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 15));
            isContain=false;
            inventoryItem.isContain = isContain;
        }
    }

    private void OnMouseEnter()
    {
        if(GUI_GameMenu.pause)return;
        if(Door.GameOverUi_pause)return;

        if (hoverType == hoverOverType.ImgInteraction)
        {
            this.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Image2;
        }

        else if (hoverType == hoverOverType.SHAKE)
        {
            SmallTools.Instance.ItemShake(this.gameObject, 0.1f, 0.1f);
        }
        else if(hoverType == hoverOverType.GameObjectInteraction)
        {
            SmallTools.Instance.ItemShake(this.gameObject, 0.1f, 0.1f);
        }
    }

    private void OnMouseExit()
    {
        if(GUI_GameMenu.pause)return;
        if(Door.GameOverUi_pause)return;

        if (hoverType == hoverOverType.ImgInteraction)
            this.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = Image1;
        
        else if(hoverType == hoverOverType.GameObjectInteraction)
        {
            SmallTools.Instance.ItemShake(this.gameObject, 0.1f, 0.1f);
        }
    }

    private void Update()
    {
        if(GUI_GameMenu.pause)return;
        if(Door.GameOverUi_pause)return;

        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit2D hit = Physics2D.Raycast(
                Camera.main.ScreenToWorldPoint(Input.mousePosition),
                Vector2.zero
            );
            if (hit.collider == null)
                return;
            if (hit.collider.name == this.gameObject.name)
            {
                if (rightClick == rightClickType.textTip)
                {
                    SmallTools.Instance.showTip(tipRightClick, 0.5f);
                }

            }
        }
    }
}
