using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverUI : MonoBehaviour
{
    public bool GameOverUi_pause; // Judgement game winning stop interaction
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(this.transform.GetChild(0).gameObject.activeSelf==true)
        {
            GameOverUi_pause=true;
        }
        if(this.transform.GetChild(0).gameObject.activeSelf==false)
        {
            GameOverUi_pause=false;
        }
    }
}
