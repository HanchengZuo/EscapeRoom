using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    public List<GameObject> items = new List<GameObject>();
    public string sceneName;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("scene controller");
        if(Inventory.SceneData.ContainsKey(sceneName))
        {
            //Update the status of objects in the scene based on inventory data
            List<InventoryItem> invs = Inventory.SceneData[sceneName];
            for (int i = 0; i < invs.Count; i++)
            {
                for (int j = 0; j < items.Count; j++)
                {
                    var item = items[j].GetComponent<Item>().inventoryItem;
                    if (item != null && invs[i] != null)
                    {
                        if (invs[i].ID == item.ID)
                        {
                            item.isPick = invs[i].isPick;
                            item.isContain = invs[i].isContain;
                            item.isOpen = invs[i].isOpen;
                        }
                    }
                }
            }
            //Initialize the updated object state
            for (int j = 0; j < items.Count; j++)
            {
                items[j].GetComponent<Item>().InitItem();
            }
            for(int j = 0; j < items.Count; j++)
            {
                var item = items[j].GetComponent<Item>();
                if(item.inventoryItem.isContain && item.containItem != null)
                {
                    item.containItem.transform.GetChild(0).gameObject.SetActive(false);
                }
                if(item.inventoryItem.isOpen && item.ObjectEnter != null & item.ObjectExit != null){
                    item.ObjectEnter.transform.gameObject.SetActive(item.inventoryItem.isOpen);
                    item.ObjectExit.transform.gameObject.SetActive(!item.inventoryItem.isOpen);
                }
            }
        }
        else
        {
            List<InventoryItem> invs = new List<InventoryItem>();
            for (int i = 0; i < items.Count; i++)
            {
                var item = items[i].GetComponent<Item>().inventoryItem;
                invs.Add(item);
            }
            Inventory.SceneData.Add(sceneName, invs);
        }
    }

    public void SaveData()
    {
        List<InventoryItem> invs = Inventory.SceneData[sceneName];
        for (int i = 0; i < invs.Count; i++)
        {
            for (int j = 0; j < items.Count; j++)
            {
                var inv = items[j].GetComponent<Item>().inventoryItem;
                if (inv != null && invs[i] != null)
                {
                    if (invs[i].ID == inv.ID)
                    {
                        invs[i].isPick = inv.isPick;
                        invs[i].isContain = inv.isContain;
                    }
                }                
            }
        }
    }
}

